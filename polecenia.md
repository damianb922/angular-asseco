git clone https://bitbucket.org/ev45ive/angular-asseco.git
cd angular-asseco
npm i

// ng s -o  

npm run start 



ng g m playlists -m app  --routing

ng g c playlists/views/playlists-view

ng g c playlists/components/items-list
ng g c playlists/components/list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

ng g m shared -m app

ng g d shared/highlight --export

ng g d shared/unless --export 


ng g m music-search -m app --routing

ng g c music-search/views/music-search-view --export

ng g c music-search/components/search-form
ng g c music-search/components/search-results
ng g c music-search/components/album-card


ng g s music-search/services/music-search 

ng g m security -m app 

ng g s security/auth 

ng g s security/auth-interceptor 


ng g c shared/card --export

ng g c shared/tabs-examples --export
ng g c shared/tabs
ng g c shared/tab
ng g c shared/tabs-nav


ng g s playlists/services/playlists 

ng g c playlists/views/selected-playlist

ng test

ng g c music-search/views/album-view 