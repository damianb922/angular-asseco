import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { AuthService } from "./auth.service";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const authRequest = req.clone({
      setHeaders: {
        Authorization: "Bearer " + this.auth.getToken()
      }
    });

    return next.handle(authRequest).pipe(
      /* Handle errors */
      catchError((error, caught) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401) {
            this.auth.authorize();
          }
          return throwError(new Error(error.error.error.message));
        }
        return throwError(error);
      })
    );
  }
}
