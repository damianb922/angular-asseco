import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import { AlbumViewComponent } from "./album-view.component";
import { By } from "@angular/platform-browser";
import { Component, Input, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";

import {
  RouterTestingModule,
  setupTestingRouter,
  SpyNgModuleFactoryLoader
} from "@angular/router/testing";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { MusicSearchService } from "../../services/music-search.service";
import { Subject, EMPTY } from "rxjs";
import { Album } from "src/app/models/Album";

// @Component({selector:'app-album-card',template:''})
// export class AlbumCardComponentMock{
//   @Input() album
// }

fdescribe("AlbumViewComponent", () => {
  let component: AlbumViewComponent;
  let fixture: ComponentFixture<AlbumViewComponent>;
  let router: Router;
  let service: jasmine.SpyObj<MusicSearchService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlbumViewComponent /* , AlbumCardComponentMock */],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: "albums/:id",
            component: AlbumViewComponent
          }
        ])
      ],
      providers: [
        {
          provide: MusicSearchService,
          useValue: jasmine.createSpyObj<MusicSearchService>("musicService", [
            "fetchAlbum"
          ])
        },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { params: { id: "123" } } }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    // router = setupTestingRouter(null,,..);
    service = TestBed.get(MusicSearchService);
    service.fetchAlbum.and.returnValue(EMPTY);

    fixture = TestBed.createComponent(AlbumViewComponent);
    component = fixture.componentInstance;
    component.album = {
      id: "123",
      name: "Test 123",
      images: [{ url: "https://www.placecage.com/gif/300/300" }]
    };
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeDefined();
  });

  it("should render album title", () => {
    // expect(fixture.nativeElement.innerText).toMatch('Placki')
    expect(findByCss("h1").nativeElement.innerText).toMatch("Test 123");
  });

  it('should expand tracks when "tracks" is clicked', async(() => {
    fixture.autoDetectChanges();

    const button = findByCss("[type=button]");
    const showTracksSpy = spyOn(component, "showTracks").and.callThrough();

    expect(findByCss(".tracks-list")).toBeNull();

    button.nativeElement.dispatchEvent(new MouseEvent("click"));

    // button.triggerEventHandler('click',{ target:  button.nativeElement, x: 100, y:200})
    // fixture.detectChanges()

    expect(findByCss(".tracks-list")).not.toBeNull(
      "Tracks list is not visible!"
    );

    expect(showTracksSpy).toHaveBeenCalled();
  }));

  it("should render album-card with album data", () => {
    const albumCard = findByCss("app-album-card");
    expect(albumCard).toBeDefined();
    expect(albumCard.properties.album).toEqual(component.album);
  });

  it("should fetch album by id from router param", inject(
    [MusicSearchService, ActivatedRoute],
    (mock: MusicSearchService, route: ActivatedRoute) => {
      const fakeAlbumResp = new Subject<Album>();

      service.fetchAlbum.and.returnValue(fakeAlbumResp);

      component.ngOnInit();

      expect(service.fetchAlbum).toHaveBeenCalledWith(
        route.snapshot.params["id"]
      );

      fakeAlbumResp.next({
        id: "123",
        name: "Album from mock service",
        images: [{ url: "https://www.placecage.com/gif/300/300" }]
      });

      expect(component.album).toEqual({
        id: "123",
        name: "Album from mock service",
        images: [{ url: "https://www.placecage.com/gif/300/300" }]
      });
    }
  ));

  function findByCss(CSSselector) {
    return fixture.debugElement.query(By.css(CSSselector));
  }
});
