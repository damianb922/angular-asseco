import { Injectable, Inject, EventEmitter } from "@angular/core";
import { Album } from "src/app/models/Album";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "src/app/security/auth.service";
import { AlbumsResponse } from "../../models/Album";
import {
  pluck,
  map,
  tap,
  catchError,
  startWith,
  distinctUntilChanged,
  mergeMap,
  concatMap,
  switchMap,
  exhaust,
  withLatestFrom
} from "rxjs/operators";
import {
  from,
  EMPTY,
  throwError,
  of,
  Subject,
  AsyncSubject,
  concat,
  ReplaySubject,
  BehaviorSubject,
  Observable
} from "rxjs";
import { SEARCH_API_URL } from "./tokens";

@Injectable({
  providedIn: "root"
  // providedIn: MusicSearchModule
})
export class MusicSearchService {
  constructor(
    @Inject(SEARCH_API_URL)
    private search_url: string,
    private http: HttpClient
  ) {
    this.queryChanges
      .pipe(
        map(query => ({ type: "album", q: query })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.search_url, { params }).pipe(
            catchError(err => {
              this.errorNotifications.next(err);
              return EMPTY;
            })
          )
        ),
        map(resp => resp.albums.items)
      )
      .subscribe(this.albumChanges);
  }

  errorNotifications = new ReplaySubject(3, 10000);

  results: Album[] = [
    {
      id: "123",
      name: "Test 123",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "234",
      name: "Test 234",
      images: [
        {
          url: "https://www.placecage.com/gif/400/400"
        }
      ]
    },
    {
      id: "345",
      name: "Test 345",
      images: [
        {
          url: "https://www.placecage.com/gif/500/500"
        }
      ]
    }
  ];

  fetchAlbum(id: string): Observable<Album> {
    return this.http.get<Album>(`https://api.spotify.com/v1/albums/${id}`)
  }

  search(query: string) {
    this.queryChanges.next(query);
  }

  private queryChanges = new BehaviorSubject<string>("batman");
  public query$ = this.queryChanges.asObservable();

  private albumChanges = new BehaviorSubject<Album[]>(this.results);
  public albums$ = this.albumChanges.asObservable();

  getAlbums() {
    return this.albumChanges.asObservable();
  }
}
