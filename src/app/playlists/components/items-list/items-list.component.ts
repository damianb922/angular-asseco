import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgIf, NgForOf, NgForOfContext } from "@angular/common";

NgIf;
NgForOf;
NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection:ChangeDetectionStrategy.OnPush
  // inputs:[
  //   'playlists:items',
  // ]
})
export class ItemsListComponent implements OnInit {

  highlight: Playlist

  @Input("items") playlists: Playlist[] = [];

  @Input() selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  constructor() {}

  ngOnInit() {}

  trackFn(index: number, item: Playlist) {
    return item.id;
  }
}
