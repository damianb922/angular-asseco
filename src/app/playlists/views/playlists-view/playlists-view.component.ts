import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { PlaylistsService } from "../../services/playlists.service";
import { ActivatedRoute, Router } from "@angular/router";
import { map, switchMap, multicast, share, shareReplay, tap, filter } from "rxjs/operators";
import { zip } from "rxjs";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  mode = "show";

  playlists$ = this.service.playlists$;
  
  selected$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get("playlist_id")),
    // Is there and ID?
    filter(id => !!id),
    map(id => parseInt(id)),
    switchMap(id => this.service.fetchPlaylist(id)),
    shareReplay()
  );

  constructor(
    private service: PlaylistsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  select(playlist_id: Playlist["id"]) {
    this.router.navigate(["/playlists", playlist_id]);
  }

  save(draft: Playlist) {
    this.service.save(draft);
    this.mode = "show";
  }

  ngOnInit() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }
}

/* 
          forkjoin({
            playlist:this.service.fetchPlaylist(playlist.id)
            user: this.usersservice.fetchusers(user.id)
          })

          zip(this.service.fetchPlaylist(playlist.id),this.usersservice.fetchusers(user.id)).pipe(
            map( ([playlist,user]) => ...)
          )

    // fetch A, then B, then use [A,B]
    queryChange.pipe(
      switchMap( a =>{
        return this.http.get('A').pipe(
              switchMap(a => {
                this.a = a;
                return this.http.get("B",{params:{a}}).pipe(
                  withLatestFrom(of(a)),
                  catchError( e => e.handleErrrors(e))
                ).
              })
              tap( ([a,b] )
            ).pipe(
              catchError( e => e.handleErrrors(e))
            ).

      })
    )

*/
