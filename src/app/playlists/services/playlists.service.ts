import { Injectable } from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { Subject, BehaviorSubject, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular Top20!",
      favorite: false,
      color: "#00ffff"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: true,
      color: "#ffff00"
    }
  ]);

  playlists$ = this.playlists.asObservable();

  constructor() {}

  fetchPlaylist(id: Playlist["id"]) {
    console.log('fetchPLaylist  ')
    const playlists = this.playlists.getValue();

    const playlist = playlists.find(
      // match id
      p => p.id === id
    );
    
    return of(playlist);
  }

  save(draft: Playlist) {
    const playlists = this.playlists.getValue();

    const index = playlists.findIndex(p => p.id === draft.id);

    if (index !== -1) {
      playlists.splice(index, 1, draft);
    }

    this.playlists.next(playlists);
  }
}
