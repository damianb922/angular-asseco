import {
  Component,
  OnInit,
  ViewChild,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { TabComponent } from "../tab/tab.component";
import { TabsNavComponent } from "../tabs-nav/tabs-nav.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.scss"]
})
export class TabsComponent implements OnInit {
  // tabs: TabComponent[] = [];

  // @ViewChild("navRef", { static: true })
  // @ViewChild(TabsNavComponent, { static: true })
  @ContentChild(TabsNavComponent, { static: true })
  nav: TabsNavComponent;

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  toggle(activeTab: TabComponent) {
    this.tabs.forEach(tab => {
      tab.open = tab === activeTab;
    });
    this.nav.active = activeTab;
  }

  constructor() {}

  ngAfterViewInit() {}

  ngAfterContentInit() {
    this.nav.tabs = this.tabs.toArray();
    this.toggle(this.tabs.toArray()[0]);

    this.tabs.toArray().forEach(tab => {
      tab.tabChange.subscribe(tab => {
        this.toggle(tab);
      });
    });
  }

  ngOnInit() {
    // console.log(this.nav, this.tabs);

    this.nav.tabChange.subscribe(tab => {
      this.toggle(tab);
    });
  }
}
